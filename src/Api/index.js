import axios from "axios";
import Auth from "./Auth";
import User from "./User";

const Api = axios.create({
  baseURL: `https://dev.api.gmeez.com/api/v1`,
});

export { Auth, User };

export default Api;
