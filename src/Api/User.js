import Api from "./index";
import Response from "./Response";

const { next } = Response;

const User = {
  getUserProfile: async (id) => {
    let endpoint = `/users/auth/user-by-id/${id}`;
    try {
      let resp = await Api.get(endpoint);
      return resp;
    } catch (e) {
      next(e.response);
    }
  },

  addUserDetails: async (id, payload, token) => {
    let endpoint = `/users/add-details/${id}`;
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    console.log(payload, "payload");
    try {
      let resp = await Api.post(endpoint, payload, config);
      return resp;
    } catch (err) {
      next(err.response);
    }
  },

  checkUsername: async (username, token) => {
    let endpoint = `/users/verifyUsername`;
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      let resp = await Api.post(endpoint, { username }, config);
      return resp;
    } catch (error) {
      next(error.response);
    }
  },
};

export default User;
