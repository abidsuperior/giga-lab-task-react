import Api from "./index";
import Response from "./Response";

const { next } = Response;

const Auth = {
  signUp: async (email, password) => {
    let endpoint = `/users/auth/signUp`;
    try {
      let response = await Api.post(endpoint, {
        email,
        password,
      });
      return response;
    } catch (err) {
      return err.response;
    }
  },

  signIn: async (email, password) => {
    let endpoint = `/users/auth/login`;
    try {
      let response = await Api.post(endpoint, {
        email,
        password,
      });
      return response;
    } catch (err) {
      return err.response;
    }
  },

  validateEmail: async (token) => {
    let endpoint = `/users/auth/verifyEmail`;

    try {
      return await Api.post(endpoint, {
        token,
      });
    } catch (e) {
      next(e.response);
    }
  },

  sendEmailVerification: async (email) => {
    let endpoint = `/users/auth/resend-code/:id`;

    try {
      return await Api.post(endpoint, {
        email,
      });
    } catch (e) {
      next(e.response);
    }
  },

  verifyEmail: async (id, code) => {
    let endpoint = `/users/auth/verify-code/${id}`;

    try {
      return await Api.post(endpoint, {
        code,
      });
    } catch (e) {
      return e.response;
    }
  },
};

export default Auth;
