import React, {useState} from 'react';
import {User} from "../Api";
import {useCookies} from "react-cookie";
import {Auth as AuthConstants} from "../Constants";

const AppContext = React.createContext({});

const AppContextProvider = (props) => {

  const cookies = useCookies([AuthConstants.JWT_TOKEN])[0];
  const token = cookies[AuthConstants.JWT_TOKEN];

  const [user, setUser] = useState({
    percentage: 0,
    name: "",
    email: "",
    image: "",
    resume: false,
    email_verified: 1
  });

  const [message, setMessage] = useState(null);
  const [toggle, setToggle] = useState(false);

  const showError = message => {
    setMessage(message);
    setToggle(!toggle);
  };

  const updateUser = () => {
    User.getProfileStatus(token).then(res => {
      setUser(res);
    }, err => {
      console.log("Profile Progress:", err);
    })
  };

  return (
    <AppContext.Provider value={{
      message,
      showError,
      setToggle,
      toggle,
      updateUser,
      user
    }}>
      {props.children}
    </AppContext.Provider>
  )
};

export {
  AppContext,
  AppContextProvider
};
