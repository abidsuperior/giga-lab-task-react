// import React, {useEffect, useState} from 'react';
// import {Courses, Jobs, Notifications, Surveys, User} from "../Api";
// import {useCookies} from "react-cookie";
// import {Auth as AuthConstants} from "../Constants";
// import randomMC from 'random-material-color';
// // import format from "date-fns/format";

// const HomeContext = React.createContext({});

// const HomeContextProvider = (props) => {

//   const cookies = useCookies([AuthConstants.JWT_TOKEN])[0];
//   const token = cookies[AuthConstants.JWT_TOKEN];

//   const [notifications, setNotifications] = useState({
//     total: 0,
//     unread: 0,
//     data: []
//   });

//   const [recommendedUpskills, setRecommendedUpskills] = useState(null);
//   const [savedUpskills, setSavedUpskills] = useState(null);
//   const [enrolledUpskills, setEnrolledUpskills] = useState(null);
//   const [completedUpskills, setCompletedUpskills] = useState(null);

//   const upskillsSetters = {
//     recommended: setRecommendedUpskills,
//     enrolled: setEnrolledUpskills,
//     saved: setSavedUpskills,
//     completed: setCompletedUpskills
//   };
//   const upskills = {
//     recommended: recommendedUpskills,
//     enrolled: enrolledUpskills,
//     saved: savedUpskills,
//     completed: completedUpskills
//   };

//   const [newSurveys, setNewSurveys] = useState([]);
//   const [completedSurveys, setCompletedSurveys] = useState([]);
//   const surveySetters = {new: setNewSurveys, completed: setCompletedSurveys};
//   const surveys = {new: newSurveys, completed: completedSurveys};

//   const [recommendedJobs, setRecommendedJobs] = useState(null);
//   const [savedJobs, setSavedJobs] = useState(null);
//   const [appliedJobs, setAppliedJobs] = useState(null);
//   const jobSetters = {recommended: setRecommendedJobs, saved: setSavedJobs, applied: setAppliedJobs};
//   const jobs = {recommended: recommendedJobs, saved: savedJobs, applied: appliedJobs};

//   const [employability, setEmployability] = useState(0);
//   const [userSkills, setUserSkills] = useState([]);
//   const [skills, setSkills] = useState(0);
//   const [total, setTotal] = useState(0);
//   const [future, setFuture] = useState(0);
//   const [ilo, setILO] = useState(0);

//   const [jobRecPage, setJobRecPage] = useState(1);
//   const [jobSavedPage, setJobSavedPage] = useState(1);
//   const [jobAppliedPage, setJobAppliedPage] = useState(1);
//   const jobPage = {
//     recommended: jobRecPage,
//     saved: jobSavedPage,
//     applied: jobAppliedPage,
//   };
//   const jobPageSetter = {
//     recommended: setJobRecPage,
//     saved: setJobSavedPage,
//     applied: setJobAppliedPage,
//   };
//   const [courseRecPage, setCourseRecPage] = useState(1);
//   const [courseEnrolledPage, setCourseEnrolledPage] = useState(1);
//   const [courseSavedPage, setCourseSavedPage] = useState(1);
//   const [courseCompletedPage, setCourseCompletedPage] = useState(1);
//   const coursePage = {
//     recommended: courseRecPage,
//     saved: courseSavedPage,
//     enrolled: courseEnrolledPage,
//     completed: courseCompletedPage,
//   };
//   const coursePageSetter = {
//     recommended: setCourseRecPage,
//     saved: setCourseSavedPage,
//     enrolled: setCourseEnrolledPage,
//     completed: setCourseCompletedPage,
//   };
//   const [jobSize, setJobSize] = useState(20);
//   const [courseSize, setCourseSize] = useState(20);

//   const mapResponseToView = {
//     upskills: upskill => ({
//       id: upskill.id,
//       title: upskill.title,
//       image: upskill.image_url,
//       description: upskill.description,
//       shortDescription: upskill.short_description,
//       link: upskill.url,
//       duration: upskill.duration,
//       author: upskill.author,
//       skillLevel: upskill.difficulty_level,
//       skills: upskill.matched_skills.split(','),
//       visited: upskill.status === 'visited' || upskill.status === 'visited_saved',
//       saved: upskill.status === 'saved' || upskill.status === 'visited_saved',
//       enrolled: upskill.status === 'enrolled',
//       completed: upskill.status === 'completed',
//     }),
//     surveys: survey => ({
//       link: survey.url,
//       visited: survey.type === 'visited',
//       completed: survey.type === 'completed',
//       ...survey
//     }),
//     jobs: job => ({
//       id: job.id,
//       company: job.company_name,
//       location: `${job.company_city}, ${job.company_country}`,
//       position: job.job_role,
//       image: job.company_image_url,
//       imageColor: randomMC.getColor({text: job.id}),
//       description: job.job_description,
//       shortDescription: job.short_description,
//       industry: job.company_industry,
//       experience: job.job_level,
//       visited: job.status === 'visited' || job.status === 'visited_saved',
//       monthlySalaryRange: job.job_salary,
//       numberOfVacancies: job.job_vacancies,
//       title: job.job_title,
//       subtitle: job.company_name,
//       link: job.job_url,
//       microtitle: job.job_experience,
//       skills: job.job_skills.split(','),
//       saved: job.status === 'saved' || job.status === 'visited_saved',
//       applied: job.status === 'applied',
//       postedDate: job.posted_date
//     }),
//     notifications: notification => ({
//       ...notification,
//       id: notification.id,
//       title: notification.payload.title,
//       description: notification.payload.description,
//       date: notification.created_at,
//     })
//   };

//   const checkSave = (item, data, setter) => {
//     let newData = [...data];
//     newData.forEach(value => {
//       if (value.id === item.id)
//         value.saved = !value.saved;
//     });
//     setter(newData);
//   };

//   const removeFromList = (item, data, setter) => {
//     setter(data.filter(value => value.id !== item.id));
//     Surveys.deleteSurvey(item.id, token).then(res => {}, err => {
//       fetchSurveys();
//     });
//   };

//   const markStatusItem = {
//     surveys: async (survey, status) => {
//       await Surveys.updateStatus(survey.id, token, status);
//       let newData = [...surveys['new']];
//       if (status === 'completed') {
//         setNewSurveys(newSurveys.filter(item => item.id !== survey.id))
//         setCompletedSurveys([...completedSurveys, survey])
//       } else {
//         newData.forEach(item => {
//           if (item.id === survey.id) {
//             item.status = status;
//             ['visited', 'completed', 'new'].map(state => item[state] = state === status);
//           }
//         });
//         surveySetters['new'](newData);
//       }
//     },
//     jobs: (job, status, {value = true} = {}) => {
//       if(status === 'visited' && job.visited)
//         return;
//       Jobs.updateStatus(job.id, token, status, {value}).then(res => {
//         fetchJobs({refresh: true})
//       });
//     },
//     upskills: (course, status, {value = true} = {}) => {
//       if(status === 'visited' && course.visited)
//         return;
//       Courses.updateStatus(course.id, token, status, {value}).then(res => {
//         fetchUpskills({refresh: true})
//       });
//     },
//   };

//   const checkSaveItem = {
//     jobs: async (job, type) => {
//       await Jobs.save(job.id, token);
//       if (job.saved)
//         removeFromList(job, savedJobs, setSavedJobs);
//       else
//         setSavedJobs([...savedJobs, job]);
//       checkSave(job, jobs[type], jobSetters[type]);
//     },
//   };

//   const removeItem = {
//     upskills: (upskill, type) => {
//       removeFromList(upskill, upskills[type], upskillsSetters[type]);
//       if(type === 'saved') {
//         Courses.updateStatus(upskill.id, token, 'saved', {value: false}).finally();
//       } else if(type === 'recommended') {
//         Courses.deleteCourse(upskill.id, token, {type}).finally();
//       }
//     },
//     jobs: (job, type) => {
//       removeFromList(job, jobs[type], jobSetters[type]);
//       if(type === 'saved') {
//         Jobs.updateStatus(job.id, token, 'saved', {value: false}).finally();
//       } else if(type === 'recommended') {
//         Jobs.deleteJob(job.id, token, {type}).finally();
//       }
//     },
//     surveys: (survey, type) => removeFromList(survey, surveys[type], surveySetters[type]),
//   };

//   const fetchJobs = ({callback=()=>{}, refresh=false, first=false} = {}) => {
//     ['recommended', 'saved', 'applied']
//       .map(type => Jobs.list(token, {type, beg: refresh ? 0 : jobPage[type], size: refresh ? jobPage[type] === 0 ? jobSize : jobSize*(jobPage[type]) : jobSize}).then(res => {
//         if (res.data.data == null || (res.data.data.length && res.data.data.length === 0))
//           return;
//         jobSetters[type]([...(refresh ? [] : jobs[type]), ...res.data.data.map(job => mapResponseToView.jobs(job))]);
//         if(!refresh && !first)
//           jobPageSetter[type](jobPage[type] + 1);
//         callback();
//       }, err => {
//         console.log("Recommended Jobs:", err);
//       }));
//   };

//   const fetchNotifications = ({refresh=false, first=false} = {}) => {
//     Notifications.list(token, {beg: 0, size: 3}).then(res => {
//       if (res.data.data == null || (res.data.data.length && res.data.data.length === 0))
//         return;
//       setNotifications({
//         unread: res.data.data.unread_count,
//         total: res.data.totalCount,
//         data: res.data.data.notifications.map(item => mapResponseToView.notifications(item))
//       });
//     }, err => {
//       console.log("Notifications:", err);
//     });
//   };

//   const loadMoreJobs = ({callback=()=>{}, type}) => {
//     Jobs.list(token, {type, beg: jobPage[type], size: jobSize}).then(res => {
//       if (res.data.data == null || (res.data.data.length && res.data.data.length === 0)){
//         callback(res.data.data.length);
//         return;
//       }
//       jobSetters[type]([...(jobs[type]), ...res.data.data.map(job => mapResponseToView.jobs(job))]);
//       jobPageSetter[type](jobPage[type] + 1);
//       callback(res.data.data.length);
//     }, err => {
//       console.log(`${type} Jobs:`, err);
//     });
//   };

//   const loadMoreCourses = ({callback=()=>{}, type}) => {
//     Courses.list(token, {type, beg: coursePage[type], size: courseSize}).then(res => {
//       if (res.data.data == null || (res.data.data.length && res.data.data.length === 0)) {
//         callback(res.data.data.length);
//         return;
//       }
//       upskillsSetters[type]([...(upskills[type]), ...res.data.data.map(upskill => mapResponseToView.upskills(upskill))]);
//       coursePageSetter[type](coursePage[type] + 1);
//       callback(res.data.data.length);
//     }, err => {
//       console.log(`${type} Upskills:`, err);
//     });
//   };

//   const fetchUpskills = ({callback=()=>{}, refresh=false, first=false} = {}) => {
//     ['recommended', 'enrolled', 'saved', 'completed']
//       .map(type => Courses.list(token, {type, beg: refresh ? 0 : coursePage[type], size: refresh ? coursePage[type] === 0 ? courseSize : courseSize*(coursePage[type]) : courseSize}).then(res => {
//       if (res.data.data == null || (res.data.data.length && res.data.data.length === 0))
//         return;
//       upskillsSetters[type]([...(refresh ? [] : upskills[type]), ...res.data.data.map(upskill => mapResponseToView.upskills(upskill))]);
//       if(!refresh && !first)
//         coursePageSetter[type](coursePage[type] + 1);
//       callback();
//     }, err => {
//       console.log("Recommended Upskills:", err);
//     }));
//   };

//   const fetchSurveys = ({callback=()=>{}} = {}) => {
//     ['new', 'completed'].map(type => Surveys.list(token, {type}).then(res => {
//       if (res.data.data == null || (res.data.data.length && res.data.data.length === 0))
//         return;
//       surveySetters[type](res.data.data.map(survey => mapResponseToView.surveys(survey)));
//       callback();
//     }, err => {
//       console.log("Surveys:", err)
//     }));
//   };

//   const markRead = (notification) => {
//     Notifications.markRead(notification.id, token).then(res => {
//       console.log(res);
//       setNotifications({
//         ...notifications,
//         unread: !notification.is_read ? notifications.unread - 1 : notifications.unread,
//       })
//     }, err => {
//       console.error("Notifications.markRead:", err);
//     });
//   };

//   useEffect(() => {
//     User.getScores(token).then(res => {
//       // User Scores
//       let scores = res['scores'];
//       setSkills(parseInt(scores['skills']));
//       setEmployability(parseInt(scores['employability']));
//       setTotal(parseInt(scores['total']));
//       setFuture(parseInt(scores['future']));
//       setILO(parseInt(scores['ilo'][0]));
//       // User Skills
//       setUserSkills(res['user_skills']);
//     }, err => {
//       console.log('Dashboard:', err);
//     });

//     fetchUpskills({refresh: true, first: true});
//     fetchSurveys();
//     fetchJobs({refresh: true, first: true});
//     fetchNotifications();
//   }, []);

//   return (
//     <HomeContext.Provider value={{
//       upskills,
//       surveys,
//       jobs,
//       removeItem,
//       checkSaveItem,
//       markStatusItem,
//       employability,
//       skills,
//       userSkills,
//       total,
//       future,
//       ilo,
//       notifications,
//       markRead,
//       fetchUpskills,
//       fetchJobs,
//       loadMoreJobs,
//       loadMoreCourses
//     }}>
//       {props.children}
//     </HomeContext.Provider>
//   )
// };

// export {
//   HomeContext,
//   HomeContextProvider
// };
