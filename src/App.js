import React, { useContext, useEffect, useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Routes from "./Utils/Routes";

import SignUpForm from "./Components/movies/SignUpForm";
import Movies from "./Components/movies/Movies";
import CreateMovie from "./Components/movies/CreateMovie";
import UpdateMovie from "./Components/movies/UpdateMovie";
import Login from "./Components/movies/SignInForm";
import { AppContext, ProfileCompletionProvider } from "./Contexts";
import { Snackbar } from "@material-ui/core";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const App = () => {
  const context = useContext(AppContext);
  const [error, setError] = useState(false);

  useEffect(() => {
    if (context.message && context.message.length > 0) {
      setError(true);
    }
  }, [context, error]);

  return (
    <MuiThemeProvider theme={THEME}>
      <ProfileCompletionProvider>
        <BrowserRouter>
          <Switch>
            {/* <Route  path="/" exact Redirect="/sign-in"  /> */}
            <Route  path="/sign-in" exact component={Login}  />
            <Route path="/sign-up" exact component={SignUpForm}   />
            <Route path="/list-movies"   exact component={Movies} />
            <Route path="/create-movie" exact component={CreateMovie}   />
            <Route path="/update-movie" exact component={UpdateMovie}   />
          </Switch>
        </BrowserRouter>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          open={context.toggle}
          onClose={() => context.setToggle(false)}
          message={context.message}
        />
      </ProfileCompletionProvider>
    </MuiThemeProvider>
  );
};

const THEME = createMuiTheme({
  typography: {
    fontFamily: `"Titillium Web", sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
  divider: {
    // Theme Color, or use css color in quote
    color: "#e9e9e9",
    backgroundColor: "#e9e9e9",
  },
  palette: {
    primary: {
      light: "#757ce8",
      main: "#232323",
      dark: "#141414",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#FFD4A0",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});

export default App;
