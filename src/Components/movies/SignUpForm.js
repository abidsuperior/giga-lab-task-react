import React, { useState } from "react";
import PropTypes from "prop-types";

import TextField from "@material-ui/core/TextField";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import IconEmail from "../../Assets/svgs/icon-email.svg";
import IconLock from "../../Assets/svgs/icon-lock.svg";
import Typography from "@material-ui/core/Typography";
//import BackgroundOnBoarding from "../Components/OnBoarding/BackgroundOnBoarding";

import * as Yup from "yup";
import { useFormik } from "formik";
import { Link,Redirect } from "react-router-dom";

import { ButtonBase, Icon } from "@material-ui/core";
import axios from "axios";
import {useHistory} from 'react-router-dom';

const token = localStorage.getItem("token");

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const SignUpForm = () => {
  const [value, setValue] = useState(1);
  const [error, setError] = useState();
  let history = useHistory();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const SignUpHandler = async (name, email, phone, password, profile) => {
    if (formik.errors.name) {
      setError(formik.errors.name);
    } else if (formik.errors.email) {
      setError(formik.errors.email);
    } else if (formik.errors.phone) {
      setError(formik.errors.phone);
    } else if (formik.errors.password) {
      setError(formik.errors.password);
    } else if (formik.errors.profile) {
      setError(formik.errors.profile);
    } else {
      setError("");
    }
    if (
      name &&
      email &&
      phone &&
      password &&
      profile &&
      !formik.errors.name &&
      !formik.errors.email &&
      !formik.errors.phone &&
      !formik.errors.password &&
      !formik.errors.profile
    ) {
      try {
        axios
          .post(
            `1.0/signup/createUser`,
            { name, email, phone, password, profile },
            {
              headers: {
                authorization: token,
              },
            }
          )
          .then(function (res) {
            console.log(res.data);
            console.log("user Created successfully");
            history.push("/sign-in");
          })
          .catch(function (error) {
            // console.log(error);
          });
      } catch (error) {
        console.log(error);
        setError(error.message);
      }
    }
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      phone: "",
      password: "",
      profile: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Name is required"),
      email: Yup.string()
        .email("Invalid email address")
        .required("Email is required"),
      phone: Yup.string().min(11).max(11).required("Phone is required"),
      // .matches(
      //   /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      //   "Must Contain 8 Characters, at-least One Uppercase, One Lowercase and One Number"
      // )

      password: Yup.string()
        .matches(
          /(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/,
          "Must Contain 8 Characters, at-least One Uppercase, One Lowercase and One Number"
        )
        .required("Password is required"),

      profile: Yup.string().required("profile is required"),
    }),
  });

  
  return (
    
      <div className="auth-wrapper">
        <div className="auth-form-wrapper">
          <div className="inner-wrapper">
            <Tabs
              className="auth-tabs"
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="secondary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Link to="/sign-in">
                <Tab label="Sign In" {...a11yProps(0)} />
              </Link>
              <Link to="/sign-up">
                <Tab className="tab-active" label="Sign Up" {...a11yProps(1)} />
              </Link>
            </Tabs>

            <TabPanel
              value={value}
              index={1}
              className="auth-tab-content"
              style={{ width: "100%" }}
            >
              {error && (
                <div className="form-message error hidden">{error}</div>
              )}
              <form noValidate className="auth-form">
                <TextField
                  className="form-input"
                  id="input-with-icon-adornment"
                  name="name"
                  fullWidth
                  required
                  placeholder="User Name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                  error={formik.touched.name && formik.errors.name}
                  inputProps={{
                    style: {
                      color: "#e9e9e9",
                    },
                  }}
                  InputProps={{
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconEmail} alt="" />
                      </Icon>
                    ),
                  }}
                />
                <TextField
                  className="form-input"
                  id="input-with-icon-adornment"
                  name="email"
                  fullWidth
                  required
                  placeholder="Email Address"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  error={formik.touched.email && formik.errors.email}
                  inputProps={{
                    style: {
                      color: "#e9e9e9",
                    },
                  }}
                  InputProps={{
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconEmail} alt="" />
                      </Icon>
                    ),
                  }}
                />

                <TextField
                  className="form-input"
                  id="input-with-icon-adornment"
                  name="phone"
                  fullWidth
                  required
                  placeholder="Phone Number"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.phone}
                  error={formik.touched.phone && formik.errors.phone}
                  inputProps={{
                    style: {
                      color: "#e9e9e9",
                    },
                  }}
                  InputProps={{
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconEmail} alt="" />
                      </Icon>
                    ),
                  }}
                />

                <TextField
                  className="form-input "
                  id="input-with-icon-adornment"
                  required
                  fullWidth
                  name="password"
                  placeholder="Password"
                  type="password"
                  autoComplete="current-password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                  error={formik.touched.password && formik.errors.password}
                  inputProps={{
                    style: {
                      color: "#e9e9e9",
                    },
                  }}
                  InputProps={{
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconLock} alt="" />
                      </Icon>
                    ),
                  }}
                />
                <TextField
                  className="form-input "
                  id="input-with-icon-adornment"
                  required
                  fullWidth
                  name="profile"
                  placeholder="Profile Details"
                  type="text"
                  autoComplete="current-password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.profile}
                  error={formik.touched.profile && formik.errors.profile}
                  inputProps={{
                    style: {
                      color: "#e9e9e9",
                    },
                  }}
                  InputProps={{
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconLock} alt="" />
                      </Icon>
                    ),
                  }}
                />
                <ButtonBase
                  className="btn btn-auth"
                  variant="contained"
                  color="primary"
                  fullWidth
                  onClick={() =>
                    SignUpHandler(
                      formik.values.name,
                      formik.values.email,
                      formik.values.phone,
                      formik.values.password,
                      formik.values.profile
                    )
                  }
                >
                  Sign Up&ensp;
                  <span position="start">
                    <ArrowForwardIcon />
                  </span>
                </ButtonBase>
              </form>
              <div className="row d-flex justify-content-center">
                <span>Already have an account?</span>
                <Link
                  className="link-redirect ml-2"
                  to="/sign-in"
                  onClick={() => {
                    setValue(0);
                  }}
                >
                  Sign In Instead
                </Link>
              </div>
            </TabPanel>
          </div>
        </div>
      </div>
    
  );
};

export default SignUpForm;
