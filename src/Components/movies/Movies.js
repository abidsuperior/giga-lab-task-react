import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { Link, Redirect, useHistory } from "react-router-dom";
import auth from "../../Constants/Auth";

import React, { useEffect, useState } from "react";

const Movies = () => {
  const [data, setData] = useState([]);
  let history = useHistory();
  let token = auth.token;

  const deleteMovie = async (id) => {
    console.log(id);
    console.log(token);
    await axios
      .delete(`1.0/movies/deleteMovie`, {
        headers: {
          authorization: token,
        },

        data: {
          id: `${id}`,
        },
      })
      .then((res) => {
        //console.log(res.data.payload);
         axios
        .get(
          `1.0/movies/listMovies`,
          {},
          {
            headers: {
              authorization: token,
            },
          }
        )
        .then((res) => {
          //console.log(res.data.payload);
          setData(res.data.payload);
          //console.log(data);
        })
        .catch((err) => console.error(err));
      })
      .catch((err) => console.error(err));
  };

  const updateMovieHandler = async (item) =>{
    history.push("/update-movie");
    auth.update_movie = item;
    console.log(auth.update_movie);

  };

   useEffect( () => {
    try {
      let token = auth.token;

      //console.log(token);
       axios
        .get(
          `1.0/movies/listMovies`,
          {},
          {
            headers: {
              authorization: token,
            },
          }
        )
        .then((res) => {
          //console.log(res.data.payload);
          setData(res.data.payload);
          //console.log(data);
        })
        .catch((err) => console.error(err));
    } catch (error) {
      console.log(error);
    }
  }, []);

  // console.log(auth.logedIn);
  // console.log(auth.token);
  if (auth.logedIn !== true) {
    return <Redirect to="/sign-in" />;
  }
  return (
    <>
      <Container fluid className="table-wraper">
        <Table striped bordered hover className="table-inner">
          <thead className="table-header">
            <tr>
              <th>#</th>

              <th className="col-wrapper">Movie Title</th>
              <th>Movie Rating</th>
              <th>Actions </th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => (
              <tr>
                <td>{item.movie_id}</td>
                <td>{item.movie_title}</td>
                <td>{item.movie_rating}</td>
                <td>
                  <Link to="/create-movie">
                    <Button variant="primary" className="btn-wrapper" >
                      Add
                    </Button>
                  </Link>
                  
                    <Button variant="secondary" className="btn-wrapper" onClick ={() => updateMovieHandler(item)}>
                      Update
                    </Button>
                 
                  <Button
                    variant="danger"
                    className="btn-wrapper"
                    onClick={() => deleteMovie(item.movie_id)}
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </>
  );
};

export default Movies;
