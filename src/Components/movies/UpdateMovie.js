import React, { useState, useEffect } from "react";

import TextField from "@material-ui/core/TextField";

import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import IconEmail from "../../Assets/svgs/icon-email.svg";

//import BackgroundOnBoarding from "../Components/OnBoarding/BackgroundOnBoarding";

import * as Yup from "yup";
import { useFormik } from "formik";
import { Link, Router, useHistory,Redirect } from "react-router-dom";

import { ButtonBase, Icon } from "@material-ui/core";
import axios from "axios";

import auth from "../../Constants/Auth";

const token = localStorage.getItem("token");

const UpdateMovie = () => {
  const [value, setValue] = useState(1);
  const [error, setError] = useState();
  const history = useHistory();

  useEffect(() => {
    try {
      //console.log(auth.update_movie);
      console.log(auth.update_movie.movie_image_url);
    } catch (error) {
      console.log(error);
    }
  }, []);

  const UpdateMovieHandler = async (id, title, rating, u_id, m_image) => {
    if (formik.errors.title) {
      setError(formik.errors.title);
    } else if (formik.errors.rating) {
      setError(formik.errors.rating);
    } else if (formik.errors.m_image) {
      setError(formik.errors.m_image);
    } else {
      setError("");
    }
    if (
      title &&
      rating &&
      !formik.errors.title &&
      !formik.errors.rating 
      
    ) {
      setError("");
      try {
        //console.log(id);
        axios
          .put(
            `1.0/movies/updateMovie`,
            {
              id: id,
              title: title,
              rating: rating,
              u_id: u_id,
              image_url: m_image,
            },
            {
              headers: {
                authorization: token,
              },
            }
          )
          .then(function (res) {
            console.log(res.data);
            history.push("/list-movies");
          })
          .catch(function (error) {
            // console.log(error);
          });
      } catch (error) {
        console.log(error);
        setError(error.message);
      }
    }
  };

  const formik = useFormik({
    initialValues: {
      id: `${auth.update_movie.movie_id}`,
      title: `${auth.update_movie.movie_title}`,
      rating: `${auth.update_movie.movie_rating}`,
      u_id: `${auth.update_movie.user_id}`,
      m_image: "",
    },
    validationSchema: Yup.object({
      title: Yup.string().required("title is required"),
      rating: Yup.string().required("Rating is required"),
    }),
  });

  if (auth.logedIn !== true) {
    return <Redirect to="/sign-in" />;
  }

  return (
    <>
      <div className="auth-wrapper">
        <div className="auth-form-wrapper">
          <div className="inner-wrapper">
            <h1 className="heading-wrapper">Update Movie</h1>

            {error && <div className="form-message error hidden">{error}</div>}
            <form noValidate className="auth-form">
              <TextField
                className="form-input"
                id="input-with-icon-adornment"
                name="title"
                fullWidth
                required
                placeholder="Movie Title"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.title}
                error={formik.touched.title && formik.errors.title}
                inputProps={{
                  style: {
                    color: "#e9e9e9",
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <Icon position="start">
                      <img src={IconEmail} alt="" />
                    </Icon>
                  ),
                }}
              />
              <TextField
                className="form-input"
                id="input-with-icon-adornment"
                name="rating"
                fullWidth
                required
                placeholder="Movie Rating"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.rating}
                error={formik.touched.rating && formik.errors.rating}
                inputProps={{
                  style: {
                    color: "#e9e9e9",
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <Icon position="start">
                      <img src={IconEmail} alt="" />
                    </Icon>
                  ),
                }}
              />

              <TextField
                className="form-input"
                id="input-with-icon-adornment"
                name="m_image"
                fullWidth
                required
                type="file"
                placeholder="Movie Image"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.m_image}
                error={formik.touched.m_image && formik.errors.m_image}
                inputProps={{
                  style: {
                    color: "#e9e9e9",
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <Icon position="start">
                      <img src={IconEmail} alt="" />
                    </Icon>
                  ),
                }}
              />

              <ButtonBase
                className="btn btn-auth"
                variant="contained"
                color="primary"
                fullWidth
                onClick={() =>
                  UpdateMovieHandler(
                    formik.values.id,
                    formik.values.title,
                    formik.values.rating,
                    formik.values.u_id,
                    formik.values.m_image
                  )
                }
              >
                Save&ensp;
                <span position="start">
                  <ArrowForwardIcon />
                </span>
              </ButtonBase>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default UpdateMovie;
