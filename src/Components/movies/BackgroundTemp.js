import React from "react";
import styled from "styled-components";

import backSVG1 from "../../Assets/svgs/gmeez-signup.svg";
import logo from "../../Assets/svgs/newlogo.svg";

import Typography from "@material-ui/core/Typography";

const BackgroundTemp = ({ Form, history }) => {
  return (
    <MainContainer>
      <BackgroundContainer>
        <TopPanel>
          <img src={logo} alt="logo" />
          <br />
          <br />
          <Typography component="p" variant="h2">
            A whole new experience of online gaming
          </Typography>
        </TopPanel>
        <MaskPanel>
          <BluredBackground />
          <Form history={history} />
        </MaskPanel>
      </BackgroundContainer>
    </MainContainer>
  );
};

const MainContainer = styled.div`
  height: 100vh;
  overflow: hidden;
`;

const BackgroundContainer = styled.div`
  background-image: url(${backSVG1});
  background-repeat: no-repeat;
  background-size: cover;
  height: 100%;
`;

const TopPanel = styled.div`
  padding: 4vh 15vh;
  color: #f1f1f1;
  width: 100vh;
  img {
    width: 20vh;
  }
`;

const MaskPanel = styled.div`
  width: 30%;
  height: 100%;
  position: fixed;
  top: 0;
  right: 0;
  z-index: 0;
  overflow: hidden;
  backdrop-filter: blur(100px);
`;

const BluredBackground = styled.div`
  width: 100%;
  height: 100%;
  display: block;
  background: url(${backSVG1}) right no-repeat;
  background-size: cover;
  background-repeat: no-repeat;
  background-size: cover;
  z-index: 0;
  position: absolute;
  filter: blur(100px);
  /* -webkit-filter: blur(4vh); */
`;

export default BackgroundTemp;
