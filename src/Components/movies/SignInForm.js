import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import IconEmail from "../../Assets/svgs/icon-email.svg";
import IconLock from "../../Assets/svgs/icon-lock.svg";
import Typography from "@material-ui/core/Typography";
import {useHistory} from 'react-router-dom';
import auth from '../../Constants/Auth';

import * as Yup from "yup";
import { useFormik } from "formik";
import { Link} from "react-router-dom";

import { ButtonBase, Icon } from "@material-ui/core";

import axios from "axios";


function TabPanel(props) {
  const { children, value, index, ...other } = props;

    return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const SignInForm = () => {
  const [value, setValue] = useState(0);
  const [error, setError] = useState();
  let history = useHistory();
  

  const SignInHandler = async (email, password) => {
    if (formik.errors.email) {
      setError(formik.errors.email);
    } else if (formik.errors.password) {
      setError(formik.errors.password);
    } else {
      setError("");
    }
    if (password && email && !formik.errors.password && !formik.errors.email) {
      try {
        axios
          .post(
            `1.0/auth/login`,
            {
              email: email,
              password: password,
            },
            {
              headers: {
                "Access-Control-Allow-Origin": "*",
              },
            }
          )
          .then((res) => {
           
            auth.token = res.data.token;
            auth.logedIn = true;
            auth.u_id = res.data.payload[0]["user_id"];
            //console.log(res.data.token);
            // console.log(res.data.payload);
            // console.log(auth.u_id);
            // localStorage.setItem("token", res.data.token);
            // localStorage.setItem("logFlag",true);
            
            history.push("/list-movies");
          })
          .catch((err) => console.error(err));
      } catch (err) {
        console.log(err.message, "err");
        setError(err.message);
      }
    }
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid email address")
        .required("Email is required"),
      password: Yup.string().required("Password is required"),
    }),
    onSubmitSignIn: async (values) => {
      await SignInHandler(values.email, values.password);
    },
  });

  return (
    <>
      <div className="auth-wrapper">
        <div className="auth-form-wrapper">
          <div className="inner-wrapper">
            <Tabs
              className="auth-tabs"
              value={value}
              indicatorColor="primary"
              textColor="secondary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Link to="/sign-in">
                <Tab className="tab-active" label="Sign In" />
              </Link>
              <Link to="/sign-up">
                <Tab label="Sign Up" />
              </Link>
            </Tabs>

            <TabPanel
              value={value}
              index={0}
              className="auth-tabs-content"
              style={{ width: "100%" }}
            >
              {error && (
                <div className="form-message error hidden">{error}</div>
              )}

              <form noValidate className="auth-form">
                <TextField
                  className="form-input "
                  id="input-with-icon-adornment"
                  name="email"
                  fullWidth
                  type="email"
                  required
                  placeholder="Email Address"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  error={formik.touched.email && formik.errors.email}
                  inputProps={{
                    style: {
                      color: "#e9e9e9",
                    },
                  }}
                  InputProps={{
                    autoComplete: "new-password",
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconEmail} alt="" />
                      </Icon>
                    ),
                  }}
                />
                <TextField
                  className="form-input "
                  id="input-with-icon-adornment"
                  required
                  fullWidth
                  name="password"
                  placeholder="Password"
                  type="password"
                  autoComplete="current-password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                  error={formik.touched.password && formik.errors.password}
                  InputProps={{
                    startAdornment: (
                      <Icon position="start">
                        <img src={IconLock} alt="" />
                      </Icon>
                    ),
                  }}
                />

                <div className="row w-100 d-flex justify-content-between mb-3">
                  <FormControlLabel
                    className="form-checkbox"
                    control={<Checkbox value="remember" color="white" />}
                    label="Remember me"
                  />
                </div>

                
                <ButtonBase
                  className="btn btn-auth"
                  variant="contained"
                  color="primary"
                  fullWidth
                  onClick={() =>
                    SignInHandler(formik.values.email, formik.values.password)
                  }
                >
                  Sign In&ensp;
                  <span position="start">
                    <ArrowForwardIcon />
                  </span>
                </ButtonBase>
              
              </form>
              <div className="row d-flex justify-content-center">
                <span>New on our platform?</span>
                <Link
                  className="link-redirect ml-2"
                  to="/sign-up"
                  onClick={() => {
                    setValue(1);
                  }}
                >
                  Create an Account
                </Link>
              </div>
            </TabPanel>
          </div>
        </div>
      </div>
    </>
  );
};

export default SignInForm;
