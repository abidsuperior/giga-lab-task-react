import React, { useState } from "react";

import TextField from "@material-ui/core/TextField";

import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import IconEmail from "../../Assets/svgs/icon-email.svg";
import {useHistory} from 'react-router-dom';

//import BackgroundOnBoarding from "../Components/OnBoarding/BackgroundOnBoarding";

import * as Yup from "yup";
import { useFormik } from "formik";
import {Redirect} from 'react-router-dom';

import { ButtonBase, Icon } from "@material-ui/core";
import axios from "axios";

import auth from '../../Constants/Auth';

const CreateMovie = () => {
  const [error, setError] = useState();
  const token = auth.token;
  const u_id = auth.u_id;
  let history = useHistory();

  const saveMovieHandler = async (title, rating, image_url) => {
    if (formik.errors.title) {
      setError(formik.errors.title);
    } else if (formik.errors.rating) {
      setError(formik.errors.rating);
    } else if (formik.errors.image_url) {
      setError(formik.errors.image_url);
    } else {
      setError("");
    }
    if (
      title &&
      rating &&
      image_url &&
      !formik.errors.title &&
      !formik.errors.rating &&
      !formik.errors.image_url
    ) {
      setError("");
      try {
        axios
          .post(
            `1.0/movies/createMovie`,
            { title, rating, image_url,u_id },
            {
              headers: {
                authorization: token,
              },
            }
          )
          .then(function (res) {
            console.log(res.data);
            history.push("/list-movies");
          })
          .catch(function (error) {
             console.log(error);
          });
      } catch (error) {
        console.log(error);
        setError(error.message);
      }
    }
  };

  const formik = useFormik({
    initialValues: {
      title: "",
      rating: "",
    },
    validationSchema: Yup.object({
      title: Yup.string().required("title is required"),
      rating: Yup.string().required("Rating is required"),
    }),
  });

  if (auth.logedIn !== true) {
    return <Redirect to="/sign-in" />;
  }
  return (
    <>
      <div className="auth-wrapper">
        <div className="auth-form-wrapper">
          <div className="inner-wrapper">
            <h1 className="heading-wrapper">Create Movie</h1>

            {error && <div className="form-message error hidden">{error}</div>}
            <form noValidate className="auth-form">
              <TextField
                className="form-input"
                id="input-with-icon-adornment"
                name="title"
                fullWidth
                required
                placeholder="Movie Title"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.title}
                error={formik.touched.title && formik.errors.title}
                inputProps={{
                  style: {
                    color: "#e9e9e9",
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <Icon position="start">
                      <img src={IconEmail} alt="" />
                    </Icon>
                  ),
                }}
              />
              <TextField
                className="form-input"
                id="input-with-icon-adornment"
                name="rating"
                fullWidth
                required
                placeholder="Movie Rating"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.rating}
                error={formik.touched.rating && formik.errors.rating}
                inputProps={{
                  style: {
                    color: "#e9e9e9",
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <Icon position="start">
                      <img src={IconEmail} alt="" />
                    </Icon>
                  ),
                }}
              />

              <TextField
                className="form-input"
                id="input-with-icon-adornment"
                name="image_url"
                fullWidth
                required
                type="file"
                placeholder="Movie Image"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.image_url}
                error={formik.touched.image_url && formik.errors.image_url}
                inputProps={{
                  style: {
                    color: "#e9e9e9",
                  },
                }}
                InputProps={{
                  startAdornment: (
                    <Icon position="start">
                      <img src={IconEmail} alt="" />
                    </Icon>
                  ),
                }}
              />

              <ButtonBase
                className="btn btn-auth"
                variant="contained"
                color="primary"
                fullWidth
                onClick={() =>
                  saveMovieHandler(
                    formik.values.title,
                    formik.values.rating,
                    formik.values.image_url
                  )
                }
              >
                Save&ensp;
                <span position="start">
                  <ArrowForwardIcon />
                </span>
              </ButtonBase>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateMovie;
